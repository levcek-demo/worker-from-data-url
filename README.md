# Load Workers from Data URLs

## Why

It is a pain to bundle worker code.  
It is also a pain to ask your users to manually copy worker code to their source directories.

## Solution

Load worker code as data URL.

### Using Rollup

You will need [@rollup/plugin-url](https://github.com/rollup/plugins/tree/master/packages/url).  
In your Rollup config file, just speficy the include pattern for your worker scripts,  
and they will be imported as data URLs.

Then you can simply initialize workers by writing:
```
import workerScriptDataUrl from "./worker.js";
const worker = new Worker(workerScriptDataUrl);
```
Business as usual.

See `rollup.config.mjs` for the rest.