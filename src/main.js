import workerDataURI from "./worker";
export const createWorker = () => new Worker(workerDataURI);