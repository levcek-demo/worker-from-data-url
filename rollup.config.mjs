import url from '@rollup/plugin-url';

export default [{
    input: "src/main.js",
    output: {
        dir: "dist",
        format: "esm"
    },
    plugins: [url({ include: /worker\.(j|t)s$/ })]
}];